#include "sorts.hpp"
#include <chrono>
#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

namespace menu {
    int poww = 4;

    void number_of_sorting() {
        int type;
        do {
            cout << "\x1B[2J\x1B[H";
            cout << "Number of element: 10 ^ " << poww << endl;
            cout << "1) Number -" << endl;
            cout << "2) Number +" << endl;
            cout << "3) Start sorting" << endl;
            cin >> type;
            switch (type) {
                case (1):
                    if (poww > 0) {
                        poww--;
                    } else {
                        cout << "\x1B[2J\x1B[H";
                        cout << "We have min number" << endl;
                    }
                    break;
                case (2):
                    poww++;
                    break;
                case (3):
                    break;
                default:
                    cout << "\x1B[2J\x1B[H";
                    cout << "Invalid input" << endl;
                    break;
            }
        } while (type != 3);
    }
}

template <typename Type>
void print_array(vector<Type> *arr) {
    for (int i : *arr) {
        cout << i << " ";
    }
}

int compare(const int &i, const int &j) { return i - j; }

vector<int> * random_vector(size_t number) {
    auto res = new vector<int>();
    for (int i = 0; i < number; ++i) {
        res->push_back(rand() % number);
    }
    return res;
}

int main() {
    auto merge = MergeSort<int>();

    menu::number_of_sorting();

    auto arr = random_vector((size_t)pow(10, menu::poww));

    printf("Start massive\n");
    print_array(arr);

    printf("\n\nSorted massive\n");
    auto res = merge.Sort(arr, compare);

    print_array(res);

    return 0;
}
