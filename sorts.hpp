#ifndef MAIN_CPP_SORTS_HPP
#define MAIN_CPP_SORTS_HPP
#include <iostream>
#include <vector>
#include <latch>
#include <thread>

template<typename Type>
class Isort {
public:
    virtual std::vector<Type> * Sort(std::vector<Type> * arr, int (*cmp)(const Type& i, const Type& j)) = 0;
};

template<typename Type>
class MergeSort : public Isort<Type>{
public:
    MergeSort() = default;

    std::vector<Type> * Sort(std::vector<Type> * arr, int (*cmp)(const Type& i, const Type& j)) override{
        std::vector<Type> * res(arr);
        mergeSort(res, 0, res->size() - 1, cmp);
        return res;
    }

private:
    void mergeSort(std::vector<Type>* buf, size_t left, size_t right, int (*cmp)(const Type& i, const Type& j))
    {
//        if(left >= right) return;
//
//        size_t middle = left + (right - left) / 2;
//
//        mergeSort(buf, left, middle, cmp);
//        mergeSort(buf, middle + 1, right, cmp);
//        merge(buf, left, right, middle, cmp);

            if(left >= right) return;

            size_t middle = left + (right - left) / 2;

            std::latch latch(2);
            std::vector<std::thread> threads;

            threads.emplace_back(
                    [&](int l, int r) {
                        mergeSort(buf, l, r, cmp);
                        latch.count_down();
                    },
                    left,
                    middle
            );

            threads.emplace_back(
                    [&](int l, int r) {
                        mergeSort(buf, l, r, cmp);
                        latch.count_down();
                    },
                    middle + 1,
                    right
            );

            for (auto &tr : threads) {
                tr.join();
            }
            latch.wait();
            merge(buf, left, right, middle, cmp);
    }

    void merge(std::vector<Type> * buf, size_t left, size_t right, size_t middle, int (*cmp)(const Type& i, const Type& j))
    {
        if (left >= right || middle < left || middle > right) return;
        if (right == left + 1 && (*buf)[left] > (*buf)[right]) {

            std::swap((*buf)[left], (*buf)[right]);
            return;
        }

        std::vector<Type> tmp(buf->begin() + left, buf->begin() + right + 1);

        for (size_t i = left, j = 0, k = middle - left + 1; i <= right; ++i) {
            if (j > middle - left) {
                (*buf)[i] = tmp[k++];
            } else if(k > right - left) {
                (*buf)[i] = tmp[j++];
            } else {
                (*buf)[i] = (cmp(tmp[j], tmp[k]) < 0) ? tmp[j++] : tmp[k++];
            }
        }
    }
};

#endif //MAIN_CPP_SORTS_HPP

